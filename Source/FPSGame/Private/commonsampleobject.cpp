// Fill out your copyright notice in the Description page of Project Settings.

#include "commonsampleobject.h"
#include "Components/SphereComponent.h"
// Sets default values
Acommonsampleobject::Acommonsampleobject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));

	RootComponent = MeshComp;


	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));

	SphereComp->SetupAttachment(MeshComp);

}

// Called when the game starts or when spawned
void Acommonsampleobject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Acommonsampleobject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

